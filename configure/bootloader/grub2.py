#!/usr/bin/env python3

import subprocess


def install(devices, *args, **kwargs):
    if isinstance(devices, str):
        devices = (devices,)

    for device in devices:
        subprocess.run(("grub2-install", device), stdin=subprocess.DEVNULL,
                       stdout=subprocess.DEVNULL)

    subprocess.run(("grub2-mkconfig", "-o", "/boot/grub/grub.cfg"))
