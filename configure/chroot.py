#!/usr/bin/env python3

import os


root_handle = os.open("/", O_RDONLY)


def chroot_target():
    os.chroot("/target")


def unchroot():
    os.fchdir(root_handle)
    os.chroot(".")
