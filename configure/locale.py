#!/usr/bin/env python3


import subprocess


def set_locale(locale, use_localegen=False):
    if use_localegen:
        # For glibc, generate the locale database
        with open("/etc/locale.gen", "w") as f:
            # TODO - support other charmaps
            f.write("{}.UTF-8 UTF-8".format(locale))

            subprocess.run(("locale-gen",), stdout=subprocess.DEVNULL)

    subprocess.run(("eselect", "locale", "set", locale + ".UTF-8"))
