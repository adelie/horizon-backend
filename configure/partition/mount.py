#!/usr/bin/env python3

import subprocess
import os


def mount(device, path, fs=None, options=None):
    args = ["mount"]

    if options:
        args.append("-o")

        optlist = []
        for option in options:
            if isinstance(option, str):
                optlist.append(option)
            else:
                optlist.append(option.join("="))

        args.append(optlist.join(","))

    if fs:
        args.extend(["-t", fs])

    args.append(path)
    args.append(device)

    subprocess.run(args)
