#!/usr/bin/env python3

import pytz
import subprocess

def get_timezones():
    """Get a dictionary of timezones."""
    tz_categories = {None: []}

    for tz in pytz.all_timezones:
        tz = tz.split("/")

        if len(tz) == 2:
            if tz[0] not in tz_categories:
                tz_categories[tz[0]] = []

            tz_categories[tz[0]].append(tz[1])
        else:
            tz_categories[None].append(tz[0])

    return tz_categories

def set_timezone(name):
    """Set a timezone based on its name."""
    subprocess.run(("eselect", "timezone", "set", name),
                   stdout=subprocess.DEVNULL, stderr=subprocess.PIPE)
